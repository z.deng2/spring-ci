# spring-ci

spring and continuous integration



http://localhost:8080/actuator/health

http://localhost:8080/actuator/httptrace


http://localhost:8080/name
http://localhost:8080/secret

// you can init multiple times to add more entries
http://localhost:8080/students/init
http://localhost:8080/students/list


docker pull registry.gitlab.com/z.deng2/spring-ci
docker run -p 8080:8080 -t registry.gitlab.com/z.deng2/spring-ci





$ docker ps
CONTAINER ID        IMAGE                                   COMMAND                  CREATED             STATUS              PORTS                    NAMES
81c723d22865        springio/gs-spring-boot-docker:latest   "java -Djava.secur..."   34 seconds ago      Up 33 seconds       0.0.0.0:8080->8080/tcp   goofy_brown


$ docker stop 81c723d22865
81c723d22865


docker rm 81c723d22865


$ docker run -e "SPRING_PROFILES_ACTIVE=prod" -p 8080:8080 -t springio/gs-spring-boot-docker
$ docker run -e "SPRING_PROFILES_ACTIVE=dev" -p 8080:8080 -t springio/gs-spring-boot-docker
$ docker run -e "JAVA_OPTS=-agentlib:jdwp=transport=dt_socket,address=5005,server=y,suspend=n" -p 8080:8080 -p 5005:5005 -t springio/gs-spring-boot-docker

