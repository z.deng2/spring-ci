package sep.spring.ci.springci;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StudentController {
    @Autowired
    private StudentRepository repository;

    @GetMapping("/students/init")
    public String initialise() {

        Student student = new Student("David", "Student No. 1");
        repository.save(student);
        student = new Student("James", "Student No. 2");
        repository.save(student);

        return "OK";
    }

    @GetMapping("/students/list")
    public Iterable<Student> list() {
        return repository.findAll();
    }
}
