package sep.spring.ci.springci;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
public class SepController {

    @GetMapping("/")
    public String home() {
        return "Welcome to Spring CI";
    }
    
    @GetMapping("/sample")
    public String sample() {
        return "New Endpoint";
    }

    @GetMapping("/name")
    public String getUsername(Principal principal) {
        return "sep-name: " + (null == principal ?
                "N/A" : principal.getName());
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping("/secret")
    public String getSecret() {
        return "TOP SECRET";
    }
}
